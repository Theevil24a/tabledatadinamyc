import React from 'react';
import Blaze from 'meteor/gadicc:blaze-react-component';
import './Blaze/dinamyc_reactive_table.js';

class ReactiveTableReactjs extends (React.Component){
	constructor(props) {
		super(props);
        this.state = {}
        
	}

    componentDidMount(){
		this.eventsTable();
		this.eventsTableRender();
    }

    eventsTable(){
		setTimeout( () => {
			$(".next-page" ).click(() => {
                this.eventsTableRender();
			});
			
			$(".previous-page" ).click(() => {
				this.eventsTableRender();
			});
			
			$(".page-number input" ).change(() => {
				this.eventsTableRender();
            });
			
			$(".reactive-table-input" ).keyup(() => {
				this.eventsTableRender();
			});

			$(".sortable" ).click(() => {
				this.eventsTableRender();
			});
		},1000)
    }
	
	eventsTableRender = () => {
		setTimeout( () => {
			let list = this.props.list_events()
			for (let item of list){
				let targets = item.target;
				for( let element of targets){
					element.addEventListener("click", item.method);
				}
			}
		},1000);
	}

	render() {
		return (
			<div>
                <Blaze className='responsive-table' template="DinamycReactiveTable" settings_table={this.props.settings_table()} tabledinamic_search={this.props.class} />	
			</div>
		);
	}
}


export default ReactiveTableReactjs