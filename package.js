Package.describe({
	name: 'theevil:reactive-table-reactjs',
	version: '0.0.11',
	summary: 'reactive-table for reactjs integration',
	git: 'https://gitlab.com/Theevil24a/tabledatadinamyc',
	documentation: 'packagereadme.md'
});

Package.onUse(function (api) {
	api.versionsFrom('1.11');
	api.add_files('Blaze/dinamyc_reactive_table.js', 'client');
	api.mainModule('reactive-table-reactjs.js', 'client');
	api.export('ReactiveTableReactjs');
});