# reactive-table for reactjs integration

## Created by [@Theevil24a](https://gitlab.com/Theevil24a) and [@ChrisNethunter](https://gitlab.com/ChrisNethunter) 

This package was created to solve a problem in the integration between a package of the blaze js library called [reactive-table](https://github.com/aslagle/reactive-table) that can be found in this route [reactive-table](https://github.com/aslagle/reactive-table) to be easily integrated natively with the events and different events of the buttons in the dynamic table when they are called by the table in a list we found an issue about that when the table is paged the events of the dynamic buttons by id the events are lost because [reactive-table](https://github.com/aslagle/reactive-table) renders new html by Some reason that we have not been able to explain in my work team is that in order to reload the events I have to use the jquery library obligatorily to be able to obtain the events of the table in case anyone wants to help solve this problem we receive applications to solve that compulsory library . Anyway, it was created to have a quick way to create and use the dynamic table library in react js with meteor js

## Usage example
```javascript
//?? simple usage method
import React from 'react';
import { Meteor } from 'meteor/meteor';
import TableDataDinamyc from 'tabledatadinamyc';

class UsersAdmin extends ( React.Component ){
	constructor(props) {
		super(props);
		this.state = {
			users: []
		}
	}

	componentDidMount(){
		this.getDataUsers();
	}

	//?? events for the table
	getListEvents = () => {
		return [
			{
				target: document.getElementsByClassName('edit-user'),
				method: this.handlerClickTest
			}
		]
	}

	//?? settings for the table
	settings_table = () => {
		return {
			//?? data list
            collection : this.state.users,
			rowsPerPage: 10,
			showNavigationRowsPerPage: false,
			showRowCount : false,
            fields: [
                
				{
					key: 'profile.name',
					label: 'id user',
				},
				
                {
                    key: '_id',
                    label: 'id user',
                },
				{
                    key: '_id',
                    label: 'Opciones',
                    fn: function (_id, object) {
                        let html = `<button data-id="${_id}" class="edit-user btn btn-dark">Editar</button>`;
                        return new Spacebars.SafeString(html);
                    },
                },
            ],
            filters: [
				'filter3',
			]
        };

	}

	getDataUsers = () => {
		Meteor.call('all.users' , ( error , response ) => {
			if(!error){
				this.setState({
					users : response
				})
			}
		});
	}
	
	render() {
		return (
			<div>
				//?? render the components send per props list of events and the list of props
				<TableDataDinamyc settings_table={this.settings_table} list_events={this.getListEvents} class="form-control col-sm-12" />
			</div>
		);
	}
}

export default UsersAdmin
```